# Foreshock analyses

This repository is associated with the publication:
> Manganiello, E., Herrmann, M., & Marzocchi, W., (2022). New physical implications from revisiting foreshock activity in southern California. *Geophysical Research Letters*, submitted.

in which we compare the foreshock activity in southern California with the prediction of the best-performing earthquake clustering model, the *Epidemic-Type Aftershock Sequences* (ETAS) model.
Why? To understand if foreshock sequences exist that behave differently than expected; and if they do, to identify them.

Specifically, we use two different declustering methods (NN & STW method, see below) to identify mainshocks and their foreshocks; then we perform different kinds of analyses (TEST1 and TEST2, see below) to assess if foreshock sequences behave as expected.

Based on these tests, we label foreshock sequences either as 'normal' or 'anomalous'. For each foreshock sequence, we extract a heat flow value from available heat flow measurements. Finally, we test if the distribution of extracted heat flow values is significantly different for normal and anomalous foreshock sequences.

Please find more information, applications, and findings in our paper.

Here you find the information and algorithms to reproduce our findings and/or apply our method to other cases (as MATLAB code, developed with MATLAB R2021b).
each script contains more information and explanations, e.g., regarding all input and output variables.

## Table of Contents

* [1. Download package and code](#1-download-package-and-code)
* [2. Load an earthquake catalog and simulate ETAS catalogs](#2-load-an-earthquake-catalog-and-simulate-etas-catalogs)
* [3. Use NN or STW method for declustering](#3-use-nn-or-stw-method-for-declustering)
* [4. Perform different analyses to find anomalous sequences](#4-perform-different-analyses-to-find-anomalous-sequences)
* [5. Prepare heat flow data](#5-prepare-heat-flow-data)
* [6. Compare locations of anomalous sequences with heat flow](#6-compare-locations-of-anomalous-foreshock-sequences-with-heat-flow)
* [References](#references)


## 1. Download package and code

Download Foreshock_analyses_package.zip functions and package necessary to run the MATLAB code.

## 2. Load an earthquake catalog and simulate ETAS catalogs

We use the southern California catalog of Hauksson et al. (2012), version _"1981-2019"_, which can be obtained from [scedc.caltech.edu/data/alt-2011-dd-hauksson-yang-shearer.html](https://scedc.caltech.edu/data/alt-2011-dd-hauksson-yang-shearer.html). But any catalog can be used as input, as long as it has the following properties for each event:

* Time (variable `time`, with *yyyy, mm, dd, hh, min, ss.sss* in separate columns)
* Latitude (variable `Lat`)
* Longitude (variable `Lon`)
* Magnitude (variable `Magn`)

To make the time property useable in MATLAB, we convert it into a relative time `time_rel` with the function [`datenum`](https://www.mathworks.com/help/matlab/ref/datenum.html):

```matlab
time = join([time(:,1), time(:,2), time (:,3), time (:,4), time (:,5), time (:,6)]);
time = datetime(time, 'InputFormat', 'yyyy-MM-dd HH:mm:ss.SSS');
time_rel = datenum(time);
```

To reproduce synthetic catalogs, we use the ETAS simulator of K. Felzer from [web.archive.org/web/20200712004939/https://pasadena.wr.usgs.gov/office/kfelzer/AftSimulator.html](https://web.archive.org/web/20200712004939/https://pasadena.wr.usgs.gov/office/kfelzer/AftSimulator.html).
Each synthetic catalog has 10 columns:

1. year (`yyyy`)
2. month (`mm`)
3. day (`dd`)
4. hour (`hh`)
5. minute (`min`)
6. second (`ss.sss`)
7. Latitude (in degrees)
8. Longitude (in degrees)
9. Depth (in km)
10. Magnitude

For example:
```
1983 1 1 3 34 11.326600 33.953447 -115.365284 2.693926 3.183011
```

All synthetic catalogs must be put in a single cell array named `Repcat`:
```matlab
Repcat = cell(1, n_synt_cat);  % n_synt_cat is the number of synthetic catalogs
```

## 3. Use NN or STW method for declustering

For declustering (i.e., identifying mainshocks and their foreshocks), two methods can be used:

### Nearest-Neighbor (NN) method

(as proposed by *Zaliapin et al. (2008)*)

* `NN_method.m`: to be used for the real catalog
* `NN_method_ETAS.m`: to be used for the ETAS catalogs

### Spatiotemporal windows (STW) method

(e.g., *Agnew and Jones 1991*; *Marzocchi and Zhuang 2011*; *Seif et al. 2019*):

* `STW_method.m`: to be used for the real catalog
* `STW_method_ETAS.m`: to be used for the ETAS catalogs


## 4. Perform different analyses to find anomalous sequences

Input variables are the same for all analyses; they can be obtained after running a script of section 3.
For each analyses, the percentile (i.e., significance level) can be specified.

* `TEST1.m`: perform TEST1, which compares the average number of foreshocks (i.e., the total number of foreshocks normalized by the number of mainshocks) observed in the real catalog with the same quantity observed in 1000 synthetic catalogs;
* `TEST1_Alternative.m`: perform an alternative analysis without normalization (actually not a statistical test);
* `TEST2.m`: perform TEST2, which compares the frequency to observe a specific number of foreshocks in the real catalog with the frequency in 1000 synthetic catalogs. This test was inspired by *Seif et al. 2017*, but modified by us. (We also plot the results based on the approach of *Seif et al. 2017*).


## 5. Prepare heat flow data

The `Heat_flow_interpolation.m` script interpolates the heat flow data.
Input variables are sampled heat flow measurements; for southern California and surrounding, we obtained those from various sources:
* [National Geothermal Data System](http://geothermal.smu.edu/static/DownloadFilesButtonPage.htm) using the data sets:
    * *Aggregated Well Data*
    * *Heat Flow Observation in Content Model Format*
    * *SMU Heat Flow Database of Equilibrium Log Data and Geothermal Wells*
    * *SMU Heat Flow Database from BHT Data*
* [RE Data Explorer](https://www.re-explorer.org/re-data-explorer/download/rede-data) for northern Mexico


## 6. Compare locations of anomalous foreshock sequences with heat flow

The `Map_heat_flow.m` script compares the distribution of heat flow values at the location of normal and anomalous foreshock sequences. To this end, it carries out two statistical tests: the two-sample Kolmogorov-Smirnov test (null hypothesis: the two distributions have the same parent distribution) and the paired Wilcoxon test (null hypothesis: the two distributions have the same median).


## References

* Agnew, D. C., & Jones, L. M. (1991). Prediction probabilities from foreshocks. *Journal of Geophysical Research, 96*(B7), 11959. doi: [10.1029/91JB00191](https://doi.org/10.1029/91JB00191)
* Hauksson, E., Yang, W., & Shearer, P. M. (2012). Waveform relocated earthquake catalog for Southern California (1981 to June 2019). *Bulletin of the Seismological Society of America, 102*(5), 2239–2244. doi: [10.1785/0120120010](https://doi.org/10.1785/0120120010)
* Marzocchi, W., & Zhuang, J. (2011). Statistics between mainshocks and foreshocks in Italy and Southern California. *Geophysical Research Letters, 38*(9), 2011GL047165. [10.1029/2011GL047165](https://doi.org/10.1029/2011GL047165)
* Seif, S., Zechar, J. D., Mignan, A., Nandan, S., & Wiemer, S. (2019). Foreshocks and Their Potential Deviation from General Seismicity. *Bulletin of the Seismological Society of America, 109*(1), 1–18. [10.1785/0120170188](https://doi.org/10.1785/0120170188)
* Zaliapin, I., Gabrielov, A., Keilis-Borok, V., & Wong, H. (2008). Clustering Analysis of Seismicity and Aftershock Identification. *Physical Review Letters, 101*, 018501. [10.1103/PhysRevLett.101.018501](https://doi.org/10.1103/PhysRevLett.101.018501)

<br>

---

Copyright © 2022 Ester Manganiello · Marcus Herrmann · Warner Marzocchi — Università degli Studi di Napoli 'Federico II'

Licensed under the [European Union Public Licence](https://joinup.ec.europa.eu/collection/eupl) ([EUPL-1.2-or-later](https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12)), the first European Free/Open Source Software (F/OSS) license. It is available in all official languages of the EU.