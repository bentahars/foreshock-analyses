%% Compute the probability to have a specific number of foreshocks (1,2,...,infinity)
% Here we consider only sequences that have foreshocks (>0)
% We compute the PMF values to have a specific number of
% foreshocks for the real catalog and for each synthetic catalog
% For the Seif's approach (Seif et al, 2019) we consider all sequences 
% (also the ones do not have foreshocks)

 function [idx_main_with_anomalous_foresh2,anomalous_sequences2,...
 idx_main_for_each_class,percent_selected,m_M,m_L] = TEST2...
 (m_M,m_L,all_previous_event_for_each_class_synt_cat,...
 all_previous_event_larger_than_0_synt_cat,...
 all_previous_event_larger_than_0,...
 idx_main_for_each_class,percent_selected,n_synt_cat)

% ---> INPUT variables are obtained after running NN_method.m or STW_method.m
% all input variables are explained in that file.m
% To set:
% percent_selected = percentile % we set 99th or 95th percentile

% OUTPUT variables
%index of anomalous mainshocks
idx_main_with_anomalous_foresh2 = cell(length(m_M),length(m_L));
%number of anomalous sequences
anomalous_sequences2 = [];


%add subfolder "extra-functions" as the main path
addpath('/extra-functions/');

%% Start analysis

%PMF for the real catalog
PMF_real_cat = cell(length(m_M),length(m_L));
%PMF for each synthetic catalog
PMF_synt_cat = cell(length(m_M),length(m_L),n_synt_cat);
%PMF for all synthetic catalog
PMF_synt_cat_all  = cell(length(m_M),length(m_L));
%maximum number of foreshocks to compute the PMF
max_numb_forsh = NaN(1,length(m_M));


% take all number of foreshocks of each class for all synthetic catalog
% consider all sequences (also with n_foreshock = 0) for Seif's approach
all_foreshocks_synt_cat = cell(length(m_M),length(m_L));

for rows = 1:length(m_M)
    for columns = 1:length(m_L)
        all_foreshocks_synt_cat(rows,columns) = ...
            {(vertcat(all_previous_event_for_each_class_synt_cat{rows,columns,:}))};
        
        %use the following variable to consider only foreshock sequences
        %{(vertcat(all_previous_event_larger_than_0_synt_cat{rows,columns,:}))};
    end
end

% Compute the probability to have a specific number of foreshocks (PMF)

for rows = 1:length(m_M)
    
    %compute the PMF value until the maximum number of foreshocks of the
    %real catalog for each magnitude class (in order to have the same 
    % x-axis length for each synthetic catalog)
    max_numb_forsh(rows) = max(all_previous_event_larger_than_0{rows,1});
    
    for columns = 1:length(m_L)
               
        %% Real Catalog
        pmf_real_cat = []; %as support to compute the PMF for each class
        for j = 1:max_numb_forsh(rows)

            % Compute the PMF of sequences with a specific number of
            % foreshocks for each main/foreshock magnite class
            pmf_real_cat = [pmf_real_cat;(sum...
                (all_previous_event_larger_than_0{rows,columns} ==j)./... %--> number of sequences with a specific number of foreshocks
                length(all_previous_event_larger_than_0{rows,columns}))]; %--> number of sequences in that class
        end
        PMF_real_cat(rows,columns) = {pmf_real_cat};%--> one single PMF value for each number of foreshock

        %% All synthetic Catalogs (Seif's approach)
        pmf_synt_cat_all = []; %as support to compute the PMF for each class
        for i = 1:max_numb_forsh(rows)
            pmf_synt_cat_all = [pmf_synt_cat_all;(sum...
                (all_foreshocks_synt_cat{rows,columns} ==i)./...%--> number of sequences with a specific number of foreshocks
                length(all_foreshocks_synt_cat{rows,columns,:}))];%--> number of sequences in that class (in all synth catalogs)
        end
        PMF_synt_cat_all(rows,columns,:) = {pmf_synt_cat_all};%--> one single PMF value for each number of foreshocks 

        %% Each synthetic catologs
        for p=1:n_synt_cat%loop on each synthetic catalog
            perc_synt_cat = []; %as support to compute the PMF for each class
            for i = 1:max_numb_forsh(rows) 
                perc_synt_cat = [perc_synt_cat;(sum...
                    (all_previous_event_larger_than_0_synt_cat{rows,columns,p} ==i)./...%--> number of sequences with a specific number of foreshocks
                    length(all_previous_event_larger_than_0_synt_cat{rows,columns,p}))];%--> number of sequences in that class (in that specific catalog)
            end
            PMF_synt_cat(rows,columns,p) = {perc_synt_cat};%--> multiple PMF values for each number of foreshocks (one for each synth catalog)
        end
       
        
    end

end

%% Reshape the variable for plot:
% take all the PMF of each class for all synthetic catalogs and put
% all together
PMF_synt_cat_each_class = cell(length(m_M),length(m_L));

for rows = 1:length(m_M)
    for columns = 1:length(m_L)
        PMF_synt_cat_each_class(rows,columns) = ...
            {vertcat(PMF_synt_cat{rows,columns,:})};

    end
end

%% Find anomalous sequences
% Compute xx-percentile 
perc_nine = cell(length(m_M),length(m_L));
anomalous_foresh = cell(length(m_M),length(m_L));

for rows = 1:length(m_M)
    for columns = 1:length(m_L)
        
        x_axis = (1:max_numb_forsh(rows));

        %% Percentile
        % Compute the xx-percentile of PMF (synthetic) values for each 
        % number of foreshocks and each forsh/mainshocks magnitude class

        anom_main = [];
        anom_foresh = [];
        perc_nov  = NaN(length(x_axis),1);

        for i = 1:length(x_axis)

            %% Compute the percentile at each number of foreshocks (i)
            appgg = cell2mat(PMF_synt_cat_each_class(rows,columns));
            
            %consider all PMF values of a specific number of foreshocs
            %i = specific number of foreshocks;
            %x_axis = the maximum number of foreshock considered in that
            %class;
            %using the following formulation, keep the PMF values of each
            %catalog for a specific number of foreshock (i.e. make a vector with all PMF (synthetic) values when i = 1, as an example)
            %REMEMBER! => lenght(x-axis) has to be the same for each
            %synthetic catalog for each foreshock class (and different for each mainshock class)
            all_PMF_values_for_a_specific_number_of_for = appgg(i:length(x_axis):length(appgg));
            %compute the percentile
            perc_nov(i) = prctile(all_PMF_values_for_a_specific_number_of_for,percent_selected);
            
            
            %% Find anomalous sequences with a PMF above xx-percentile
            appp = PMF_real_cat{rows,columns}'; 

            %find anomalous sequences until the max number of foreshocks
            %of the real catalog
            if perc_nov(i) < appp(i)%compare the percentile of each
                %number of foreshocks with the PMF value of the real
                %catalog
                
                %if PMF values of real catalog is larger than xx-percentile
                %for a specific number of foreshocks --> 
                % anomalous sequences are the ones with that specific
                % number of foreshock
                app = all_previous_event_larger_than_0{rows,columns};%<-- look at number of foreshocks for each class
                app = find(app == i);%find sequences with that specific number of foreshocks (when null hypothesis is rejected)
                app2 = idx_main_for_each_class{rows,columns};%<---find mainshocks with that num_of foreshocks
                app2 = app2(app);
                anom_main = [anom_main; app2];%count anomalous mainshocks
                anom_foresh = [anom_foresh; i];%save anomalous foreshocks

            end
        end

        %save anomalous sequences and their n_foreshocks for each class
        idx_main_with_anomalous_foresh2(rows,columns) = {anom_main};
        anomalous_foresh(rows,columns) = {anom_foresh};
        %save the percentile for each class for each number of foreshocks    
        perc_nine(rows,columns) = {perc_nov};
        

    end
end
%count the total number of anomalous sequences from this test
anomalous_sequences2 = unique(vertcat(idx_main_with_anomalous_foresh2{:,:}));

%% Figure TEST2

%Plot figure
figure('Position',[1 1 1000 1000],'Color',[1 1 1]);
tlo = tiledlayout(length(m_M),length(m_L),'Padding', 'none', 'TileSpacing', 'compact');
range_on_figures = (1:length(m_L):length(m_L)*length(m_M));

nexttile()
c = 0; %number of cicles
raw_data = cell(length(m_M),length(m_L));

for rows = 1:length(m_M)
    for columns = 1:length(m_L)
        y_for_plot = [];
        x_for_plot = [];
        x_axis = 1:max_numb_forsh(rows);
        x_axis_synt_cat = repmat(x_axis,1,n_synt_cat);
        c = c+1;
        ax = nexttile(c);

        %Loop to put the title above the first 4 subplot
        if c>=1 && c<=length(m_L)
            Title_names = join(["m_F ≥ ", m_L(columns)]);
            title(Title_names,'FontWeight','bold','FontSize',12)
        end

        %Loop to put the y axis only to the left subplot
        if c == range_on_figures(end)
            y_label_names_last = join(sprintf('m_M ≥ %.1f',m_M(end)));
            ylabel(y_label_names_last,'FontWeight','bold','FontSize',12)
        elseif ismember(c,range_on_figures)
            y_label_names = join(sprintf('%.1f ≤ m_M < %.1f',m_M(rows),m_M(rows+1)));
            ylabel(y_label_names,'FontWeight','bold','FontSize',12)
        end

        nexttile(c)
        y_for_plot = PMF_synt_cat_each_class{rows,columns}...
            (PMF_synt_cat_each_class{rows,columns}>0);
        x_for_plot = x_axis_synt_cat(PMF_synt_cat_each_class{rows,columns}>0);
            
        %----swarm plot
        
        %use the swarm plot to obtain the coordinate of jittered points
        s = swarmchart(x_for_plot,y_for_plot);
        %s.XJitterWidth
        raw_s = struct(s);
        raw_data_app = raw_s.XYZJittered;
        %remove original x values from the x-jittered values
        x_reljittered = raw_data_app(:,1)-x_for_plot';
        %rescaled x jittered values
        wight_swam = [0.5; 0.4; 0.3; 0.2; 0.2];
        raw_data_app(:,1) = (x_reljittered/max(x_reljittered)*wight_swam(rows))+x_for_plot';

        %Plot PMF values of each catalog with jittered values
        scatter(raw_data_app(:,1),y_for_plot,1,...
            '.','MarkerEdgeColor',[0.7 0.7 0.7])
        hold on

        %-----------------------

        %Plot Percentile with horizontal vertical lines
         x_axis = 1:max_numb_forsh(rows);
        scatter(x_axis,perc_nine{rows,columns},80,'Marker','_',...
            'MarkerEdgeColor',[0.3 0.3 0.3],'LineWidth',1)
        hold on

        %Plot all synthetic catalog (Seif's approach)
        scatter(x_axis,PMF_synt_cat_all{rows,columns},...
            12,[0 0.4470 0.7410],'o','LineWidth',0.7);
        hold on

        %Plot real catalog data
        scatter(x_axis,PMF_real_cat{rows,columns},40,'^',...
            'MarkerFaceColor','k','MarkerEdgeColor','k')
        hold on

        %Plot real catalog with anomalous foreshocks
        color_for_anom = hex2rgb('#ec3c0d'); % REF. for function:Chad Greene (2022). 
        % rgb2hex and hex2rgb (https://www.mathworks.com/matlabcentral/fileexchange/46289-rgb2hex-and-hex2rgb),
        % MATLAB Central File Exchange. Retrieved March 12, 2022.
        
        appogg_num = PMF_real_cat{rows,columns}(anomalous_foresh{rows,columns});
        scatter(anomalous_foresh{rows,columns},...
            (appogg_num),80,color_for_anom,'^','filled')
        hold on

        %Set axis
        set(gca, 'YScale', 'log')
        set(gca, 'XScale', 'log')
        ylim ([0.00001 1])
        yticks([0.00001 0.001 0.1 1])
        
        %Loop to put the title above the first 4 subplot
        if c>=1 && c<=length (m_L)
            Title_names = join(["m_F ≥ ", m_L(columns)]);
            title(Title_names,'FontWeight','bold','FontSize',12)
        end

        %Loop to put the y axis only to the left subplot
        if c == range_on_figures(end)
            y_label_names_last = join(sprintf('m_M ≥ %.1f',m_M(end)));
            ylabel(y_label_names_last,'FontWeight','bold','FontSize',12)
        elseif ismember(c,range_on_figures)
            y_label_names = join(sprintf('%.1f ≤ m_M < %.1f',m_M(rows),m_M(rows+1)));
            ylabel(y_label_names,'FontWeight','bold','FontSize',12);
        end


    end
end

xlabel(tlo, 'Number of foreshocks, N_F','FontWeight','bold','FontSize',12)
ylabel(tlo, 'PMF','FontWeight','bold','FontSize',12)

lg  = legend('ETAS (this study)',join(sprintf('ETAS %g^t^h percentile',percent_selected)),...
    'Approach of Seif et al. 2019',...
    'Real catalog',join(sprintf('Real catalog above %g^t^h percentile',percent_selected)),...
    'Orientation','Horizontal','FontSize',11);
lg.Layout.Tile = 'North'; % <-- Legend placement with tiled layout


end