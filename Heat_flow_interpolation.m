%% Heat flow interpolated map

% Load heat flow mesuraments (mu*W/M^2) and their location: Latitude and
% Longitude of the sampled points; 
% Here the range of the sampled point is Latitude = [30; 39], Longitude = [-123 -113].

%after running, the function shows an interpolated heat flow map

function [alpha_grid, gridx, Gridx, gridy, Gridy, HF_grid, x_grid_input, y_grid_input] = ...
    Heat_flow_interpolation (Lat_heat_flow,Long_heat_flow,Value_heat_flow)

% INPUT variables:
% Lat_heat_flow = Latitude (degree);
% Long_heat_flow = Longitude (degree);
% Value_heat_flow = heat flow values [muW/m^2];

% OUTPUT variables:
% alpha_grid = trasparency;
% gridx = Longitude vector;
% Gridx = Longitude matrix;
% gridy = Latitude vector;
% Gridy = Latitude matrix;
% HF_grid = interpolated heat flow values;
% x_grid_input = longitude of heat flow values;
% y_grid_input = latitude of heat flow values;

%add subfolder "extra-functions" as the main path
addpath('/extra-functions/');

%% 1 Make data set
 
%remove NaN values
app = isnan(Value_heat_flow);
Lat_heat_flow(app) = [];
Long_heat_flow(app) = [];
Value_heat_flow(app) = [];

%only unique data
[Long_heat_flow,idx_long] = sort(Long_heat_flow);
Lat_heat_flow = Lat_heat_flow(idx_long);
Value_heat_flow = Value_heat_flow(idx_long);


%mean values of common points (if sampled points are from different data set)
c = Lat_heat_flow+Long_heat_flow;
for i = 1:length(Lat_heat_flow)
    a = [];
    a = c(:)==c(i);
    mean_value_heat_flow(i) = mean(Value_heat_flow(a));
end
   mean_value_heat_flow = mean_value_heat_flow'; 

% only unique values
all_data = [Long_heat_flow Lat_heat_flow mean_value_heat_flow];
[A,idx] = unique(all_data(:,[1 2]),'rows');
all_data = [A(:,1) A(:,2) all_data(idx,3)];

%only real data 
x_grid_input = all_data(:,1);
y_grid_input = all_data(:,2);
z = all_data(:,3);
clearvars -except x_grid_input y_grid_input z

% make grid
gridx = linspace(min(x_grid_input),max(x_grid_input),length(x_grid_input));
gridy = linspace(min(y_grid_input),max(y_grid_input),length(y_grid_input));
[Gridx,Gridy] = meshgrid(gridx,gridy);

%% 2 Interpolated heat flow values

% compute heat flow value for each point of a grid 
alpha_grid = NaN(length(x_grid_input), length(y_grid_input)); % stores alpha map
HF_grid = NaN(length(x_grid_input), length(y_grid_input));                                 
Lat_km = 110.574;
R = 20; %radius of interpolation
kernel_G_BW = R / 2; % Kernel bandwidth for Gaussian kernel
kernel_G_denom = 2 * kernel_G_BW^2;  % Denominator for Gaussian kernel
kernel_PL_num = R^3;  % Normalizing factor for powerlaw kernel

kernel_G_BW_a = R / 2;
kernel_G_denom_a = 2 * kernel_G_BW_a^2;

% Loop over grid nodes (average heat flow measurements, calculate distances for alpha map)

for i = 1:length(gridy)

    Lon_km = 111.320 * cos(gridy(i) * pi/180);

    for j = 1:length(gridx)

        current_vals = []; % store all measurements related to current grid node
        current_alphas = []; % store all alphas at current grid node
        current_Ws = []; % store the distant-dependent weights

        % Loop over all measurements
        for k = 1:length(z)

            % Calculate distance components [km]
            x_diff_km = (Gridx(i,j) - x_grid_input(k)) * Lon_km;
            y_diff_km = (Gridy(i,j) - y_grid_input(k)) * Lat_km;


            dist = sqrt(x_diff_km^2 + y_diff_km^2);
            if dist <= R  % if measurement within radius ...
                current_vals = [current_vals; z(k)]; % add it

                weight_gaussian = exp(-dist^2 / kernel_G_denom);  % weighted based on Gaussian kernel [Helmstetter et al. 2007]
                current_Ws = [current_Ws; weight_gaussian]; % add weight for weighted average                
              
                weight_gaussian_a = exp(-dist^2 / kernel_G_denom_a);
                current_alphas = [current_alphas; weight_gaussian_a];                          
            end
        end
        
        HF_grid(i,j) = sum(current_vals .* current_Ws) / sum(current_Ws); % fill heat flow matrix (weighted average)
        alpha_grid(i,j) = 1 - prod(1 - current_alphas); % fill alphamap matrix (multiplicative)

    end
end

%Scale transparency overall
global_alpha = 0.8;
alpha_grid = alpha_grid * global_alpha;

%% 3 Interpolated heat flow map

ax = nexttile(1);
h = surf(Gridx, Gridy, HF_grid,...
         'Parent', ax, 'FaceAlpha', 'flat',...
         'AlphaData', alpha_grid);
set(h,'edgecolor','none');
view(ax,[0,90]);         


hold on 
plot3(x_grid_input,y_grid_input,max(HF_grid)+8000,'.','MarkerFaceColor','k','MarkerEdgeColor','k','MarkerSize',1);

hold on
borders('california','k','linewidth',0.1)
hold on
borders('mexico','k','linewidth',0.1)
hold on
borders('nevada','k','linewidth',0.1)
hold on
borders('arizona','k','linewidth',0.1) % Ref. for borders function:Greene, 
% Chad A., et al. “The Climate Data Toolbox for MATLAB.” 
% Geochemistry, Geophysics, Geosystems, American Geophysical Union (AGU), 
% July 2019, doi:10.1029/2019gc008392.


xlim([-122.5 -113]) 
ylim([30.5 38])
lat_lon_proportions  % Ref. for lat_lon_proportions function: Jonathan Sullivan (2022).
% Correctly proportion a lat/lon plot 
% (https://www.mathworks.com/matlabcentral/fileexchange/32462-correctly-proportion-a-lat-lon-plot), 
% MATLAB Central File Exchange. Retrieved March 12, 2022.


cb = colorbar;
colormap('parula')
caxis([20 170])
cbarrow; % Ref. for cbarrow function: Chad Greene (2022). cbarrow: pointy ends for colorbars 
% (https://www.mathworks.com/matlabcentral/fileexchange/52515-cbarrow-pointy-ends-for-colorbars), 
% MATLAB Central File Exchange. Retrieved March 12, 2022.

cb.Label.String = 'Heat flow, \muW/m^2';
cb.Ticks = 20:10:170;
cb.TickLabels = num2cell(20:10:170);
title('heat flow map')

lgnd = legend('','Sampled points','Location','southwest');
temp = [lgnd; lgnd.ItemText];
set(temp, 'FontSize', 7.5)

end
