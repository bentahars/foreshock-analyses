%% ANOMALOUS SEQUNECES AND NORMAL SEQUENCES WITH HEAT FLOW VALUES

function Map_heat_flow(idx_main_with_anomalous_foresh,...
  idx_main_for_each_class,m_M,m_L,Long,Lat,Magn,...
  alpha_grid, Gridx, Gridy, HF_grid, x_grid_input, y_grid_input,z)

% INPUT variables:
% - m_M,m_L,Long,Lat,Magn --> original input values explained in TEST1 or
% TEST2
% - idx_main_with_anomalous_foresh, idx_main_for_each_class --> they
% are obtained after running TEST1.m or TEST2.m
% - alpha_grid, Gridx, Gridy, HF_grid, x_grid_input, y_grid_input,z --> they 
% are obtained after running Heat_flow_interpolation.m


%% 1. Make variables for plot
% Nomal mainshocks for each mainshock and foreshock magnitude class
normal_mainsh_for_each_class = cell(length(m_M),length(m_L));
for i = 1:length(m_M)
    for j = 1:length(m_L)
        normal_mainsh_for_each_class{i,j} = setdiff...
            (idx_main_for_each_class{i,j},idx_main_with_anomalous_foresh{i,j});
    end
end

% make a vector with anomalous mainshocks
anomalous_mainshocks = unique(vertcat(idx_main_with_anomalous_foresh{:}));
if isempty(anomalous_mainshocks)
    error('No anomalous foreshock sequences')
end

% make a vector with all mainshocks with foreshocks
idx_mains_with_foresh = unique(vertcat(idx_main_for_each_class{:,:}));
% make a vector with normal mainshocks with foreshocks
normal_mainshocks = setdiff(idx_mains_with_foresh,anomalous_mainshocks);

% --- 1.1 interpolate heat flow value for each anomalous and normal mainshock
% assign an heat flow value to each normal mainshock
HF_value_for_norm_main = NaN(1,length(normal_mainshocks));
% ssign an heat flow value to each anomalous mainshock
HF_value_for_anom_main = NaN(1,length(anomalous_mainshocks));


% Loop on anomalous mainshock
Lat_km = 110.574; % Latitude-to-km conversion factor (for coordinate differences)
R = 20; %radius of interpolation
kernel_G_BW = R / 2; % Kernel bandwidth for Gaussian kernel
kernel_G_denom = 2 * kernel_G_BW^2;  % Denominator for Gaussian kernel
kernel_PL_num = R^3;  % Normalizing factor for powerlaw kernel
kernel_G_BW_a = R / 2;
kernel_G_denom_a = 2 * kernel_G_BW_a^2;

% Loop over each anomalous sequences
for i = 1:length(anomalous_mainshocks)

    Lon_km = 111.320 * cos(Lat(anomalous_mainshocks(i)) * pi/180);

        current_vals = []; % store all measurements related to current grid node
        current_alphas = []; % store all alphas at current grid node
        current_Ws = []; % store the distant-dependent weights

        % Loop over all measurements
        for k = 1:length(z)

            % Calculate distance components [km]
            x_diff_km = (Long(anomalous_mainshocks(i)) - x_grid_input(k)) * Lon_km;
            y_diff_km = (Lat(anomalous_mainshocks(i)) - y_grid_input(k)) * Lat_km;
            
            dist = sqrt(x_diff_km^2 + y_diff_km^2);

            if dist <= R  % if measurement within radius ...
                current_vals = [current_vals; z(k)]; % add it

                weight_gaussian = exp(-dist^2 / kernel_G_denom);  % weighted based on Gaussian kernel [Helmstetter et al. 2007]
                current_Ws = [current_Ws; weight_gaussian]; % add weight for weighted average                
                         
            end
        end
        
        HF_value_for_anom_main(i) = sum(current_vals .* current_Ws) / sum(current_Ws); % fill heat flow matrix (weighted average)

end


% Loop on normal sequences

for i = 1:length(normal_mainshocks)

    Lon_km = 111.320 * cos(Lat(normal_mainshocks(i)) * pi/180);

        current_vals = []; % store all measurements related to current grid node
        current_alphas = []; % store all alphas at current grid node
        current_Ws = []; % store the distant-dependent weights

        % Loop over all measurements
        for k = 1:length(z)

            % Calculate distance components [km]
            x_diff_km = (Long(normal_mainshocks(i)) - x_grid_input(k)) * Lon_km;
            y_diff_km = (Lat(normal_mainshocks(i)) - y_grid_input(k)) * Lat_km;
            
            dist = sqrt(x_diff_km^2 + y_diff_km^2);

            if dist <= R  % if measurement within radius ...
                current_vals = [current_vals; z(k)]; % add it

                weight_gaussian = exp(-dist^2 / kernel_G_denom);  % weighted based on Gaussian kernel [Helmstetter et al. 2007]
                current_Ws = [current_Ws; weight_gaussian]; % add weight for weighted average                
                        
            end
        end
        
        HF_value_for_norm_main(i) = sum(current_vals .* current_Ws) / sum(current_Ws); % fill heat flow matrix (weighted average)
 
end

%% 2 Figure of normal and anomalous mainshock location on interpolated heat flow map

figure('Position',[1 0.5 1000 500],'Color',[1 1 1]);

% axes property
ax1 = axes('Position',[0.025 0.1 0.5 0.88]);
grid on
view(ax1,[0,90]);
xlim([-123.5 -113])
ylim([30.5 38])
lat_lon_proportions
hold on

% Heat flow map 3D Plot to show in 2D
ax2 =axes('Position',[0.025 0.1 0.5 0.88]);
h = surf(ax2,Gridx, Gridy, HF_grid,...
    'Parent', ax2, 'FaceAlpha', 'flat',...
    'AlphaData', alpha_grid);
set(h,'edgecolor','none');
view(ax2,[0,90]);
hold on

% Heat flow sampled points (+8000 due to 3D plot, to show measuraments above heat flow values)
p1 = plot3(ax2,x_grid_input,y_grid_input,max(HF_grid)+8000,'.',...
    'MarkerEdgeColor',[0.3 0.3 0.3],'MarkerSize',8);
view(ax2,[0,90]);
hold on

% Marker for anomalous and normal foreshocks sequences in the legend
hold on
p2 = plot(nan,nan,'o','MarkerEdgeColor','k',...
    'LineWidth',1,'MarkerSize',8);
hold on
p3 = plot(nan,nan,'o','MarkerEdgeColor','k','MarkerFaceColor',...
    'k','LineWidth',1,'MarkerSize',8);
hold on

clear rows columns
[rowsnum,columnsnum] = size(idx_main_with_anomalous_foresh);
string_NS = [];
print_index = [];

% Plot normal sequences
for rows = 1:rowsnum
    for columns = columnsnum:-1:1
        
        hold on
        %NS
        if isempty(normal_mainsh_for_each_class{rows,columns}) == 0
            p5(rows,columns) = plot3(ax2,Long(normal_mainsh_for_each_class{rows,columns}), ...
                Lat(normal_mainsh_for_each_class{rows,columns}),...
                Magn(normal_mainsh_for_each_class{rows,columns})+9000, ...
                'o','MarkerEdgeColor','k',...
                'LineWidth',1,'MarkerSize',(rows+(0.7*rows)+4));
            % string for legend
            if rows == length(m_M)
                string_NS = [string_NS; sprintf(("    m_M ≥ %.1f"), m_M(rows))];
            else
                string_NS = [string_NS; sprintf(("    %.1f ≤ m_M < %.1f"), m_M(rows),m_M(rows)+0.5)];
            end
        end

    end
end

%Plot anomalous sequences
for rows = 1:rowsnum
    for columns = columnsnum:-1:1
        %AS
        hold on
        if isempty(idx_main_with_anomalous_foresh{rows,columns})
            p4(rows,columns) = plot(nan,nan,'o','MarkerEdgeColor','w','MarkerFaceColor',...
                'w','LineWidth',0.5,'MarkerSize',5);
            app(rows,columns) = 0;
        else
            p4(rows,columns) = plot3(ax2,Long(idx_main_with_anomalous_foresh{rows,columns}), ...
                Lat(idx_main_with_anomalous_foresh{rows,columns}),...
                Magn(idx_main_with_anomalous_foresh{rows,columns})+9000, ...
                'o','MarkerEdgeColor','k','MarkerFaceColor',...
                'k','LineWidth',1,'MarkerSize',(rows+(0.7*rows)+4));
            app(rows,columns) = 1;
        end
hold on
    end

    % index for legend
    if isempty(find(app(rows,:),1,'first'))
        print_index = [print_index; [rows,1]];
    else
        print_index = [print_index; [rows, find(app(rows,:),1,'first')]];
    end
    p_for_AS(rows,1) =  p4(print_index(rows,1),print_index(rows,2));
end

script_dim = 12;
string_NS = unique(string_NS);
string_AS = [" ";" ";" ";" ";" "];

% Borders
hold on
borders('california','k','linewidth',0.1)
hold on
borders('mexico','k','linewidth',0.1)
hold on
borders('nevada','k','linewidth',0.1)
hold on
borders('arizona','k','linewidth',0.1)% Ref. for borders function:Greene, 
% Chad A., et al. “The Climate Data Toolbox for MATLAB.” 
% Geochemistry, Geophysics, Geosystems, American Geophysical Union (AGU), 
% July 2019, doi:10.1029/2019gc008392.

xlim([-123.5 -113])
ylim([30.5 38])
lat_lon_proportions % Ref. for lat_lon_proportions function: Jonathan Sullivan (2022).
% Correctly proportion a lat/lon plot 
% (https://www.mathworks.com/matlabcentral/fileexchange/32462-correctly-proportion-a-lat-lon-plot), 
% MATLAB Central File Exchange. Retrieved March 12, 2022.
%%%Link them together
linkaxes([ax1,ax2])

%%Hide the top axes
ax2.Visible = 'off';
ax2.XTick = [];
ax2.YTick = [];

%%Give each one its own colormap
colormap(ax2,'parula')

%Heat flow colorbar parula
cb2 = colorbar(ax2,'Position',[.535 .1 .0175 .88]);
caxis([20 170])
cbarrow;% Ref. for cbarrow function: Chad Greene (2022). cbarrow: pointy ends for colorbars 
% (https://www.mathworks.com/matlabcentral/fileexchange/52515-cbarrow-pointy-ends-for-colorbars), 
% MATLAB Central File Exchange. Retrieved March 12, 2022.
cb2.Label.String = 'Heat flow, \muW/m^2';
cb2.FontSize = script_dim;
cb2.Ticks = 20:10:170;
cb2.TickLabels = num2cell(20:10:170);

% ------ Legend for NS
lgnd_1 = legend([h(1) p1(1) p2(1) p3(1) (p5(1:length(p5)))],...
    ["Interpolated heat flow";"Heat flow measurement";...
    "Normal sequences";"Anomalous sequences";string_NS],'FontSize',script_dim);
lgnd_1.Position = [.032 .12 0.17 .37];

% ------- Legend for AN
a=axes('position',get(gca,'position'),'visible','off');
hold on
lgnd_2 = legend(a,p_for_AS(1:length(p_for_AS)),string_AS,'Location',...
    'southwest','FontSize',script_dim);
lgnd_2.Position = [.031 .118 0.1 .24];
lgnd_2.Box = 'off';

%-------- String (a) above first figure
annotation('textbox','String','(a)','Color','k','FontSize',15,...
    'Position', [.042 .09 .01 .88],'FontWeight','bold', ...
    'HorizontalAlignment','right','Vert','top','FitBoxToText','on',...
    'EdgeColor','none');

%% 2. ECDF Empirical cumulative distribution function

% ecdf for heat flow value for anomalous and normal mainshock sequences
HF_value_for_anom_main = HF_value_for_anom_main(HF_value_for_anom_main>0);
HF_value_for_norm_main = HF_value_for_norm_main(HF_value_for_norm_main>0);
hold on
ax3 = axes('Position',[0.67 0.1 0.325 0.88]); %[0.72 0.205 0.27 0.53]
ecdf(HF_value_for_norm_main)
hold on
ecdf(HF_value_for_anom_main)
hold on

h_ecdf = get(gca,'children');
set(h_ecdf(1),'color','k','Linestyle','-','LineWidth',0.8);
set(h_ecdf(2),'color','k','Linestyle','--','LineWidth',0.8);

% 2.1 Two-sample Kolmogorov-Smirnov test
% Returns a test decision for the null hypothesis that the data in vectors
% x1 and x2 are from the same continuous distribution
[h_Kolmog,p_Kolmog] = kstest2(HF_value_for_norm_main,...
    HF_value_for_anom_main,'Alpha',0.01);

% 2.2 Wilcoxon rank sum test
% Ranksum tests the null hypothesis that data in x and y are samples from
% continuous distributions with equal medians, against the alternative
% that they are not.
[p_Wilcoxon,h_Wilcoxon] = ranksum(HF_value_for_norm_main,...
    HF_value_for_anom_main,'Alpha',0.01);

%Annotation KM and Wilcoxon test
if p_Wilcoxon > 0.001 
str = {'','','','','','','Kolmogorov-Smirnov test',...
    sprintf('p-value = %.3f',round(p_Kolmog,3,'significant')),...
    '','Wilcoxon rank sum test',...
    sprintf('p-value = %.3f',round(p_Wilcoxon,3,'significant')),'',...
};
else
  str = {'','','','','','','Kolmogorov-Smirnov test',...
    sprintf('p-value = %.e',round(p_Kolmog,3,'significant')),...
    '','Wilcoxon rank sum test',...
    sprintf('p-value = %.e',round(p_Wilcoxon,3,'significant')),'',...
    };
end
annotation('textbox','String',str,'Color','k','FontSize',script_dim,...
    'Position', ax3.Position, ...
    'HorizontalAlignment','right','Vert','middle','FitBoxToText','on',...
    'EdgeColor','none');

%Axes & legend of cdf
 legend(sprintf('Normal foreshock sequences (N = %.f)',...
     length(HF_value_for_norm_main)),...
     sprintf('Anomalous foreshock sequences (N = %.f)',...
     length(HF_value_for_anom_main)),...
     'Location','southeast','FontSize',script_dim);
 set(gca,'XScale','log')
xlabel('Heat flow, \muW/m^2','FontSize',script_dim)
ylabel('ECDF','FontSize',script_dim)
ax3.FontSize = script_dim;

%-------- String (b) above first figure
annotation('textbox','String','(b)','Color','k','FontSize',15,...
    'Position', [.69 .09 .01 .88],'FontWeight','bold', ...
    'HorizontalAlignment','right','Vert','top','FitBoxToText','on',...
    'EdgeColor','none');

%remove upper and right tick on x-y axes
a = gca;
% set box property to off and remove background color
set(a,'box','off','color','none')
% create new, empty axes with box but without ticks
b = axes('Position',get(a,'Position'),'box','on','xtick',[],'ytick',[]);
% set original axes as active
axes(a)
% link axes in case of zooming
linkaxes([a b])


end