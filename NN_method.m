%% Declustering using NN-method for the real catalog
% This script uses the NN formulation of Zaliapin et al., (2008) to find 
% the parent of each event

function [idx_all_main_for_each_class,...
    all_previous_event_for_each_class,idx_main_for_each_class,...
    all_previous_event_larger_than_0,n_mainshocks_rc,...
    n_foreshocks_rc,n_foreshocks_real,m_M,m_L]  = ...
    NN_method(Lat, Long, Magn, time_rel, d,b,m_L,m_M, m_main,spacing)

% INPUT variables: 
% Lat = latitude of each event (degree)
% Long = longitude of each event (degree)
% Magn = magnitude of each event
% m_M = mainshock magnitude class %we set [4:0.5:6] note the last class has
%       not a right edge (all mainshocks M>6 are part of the last class). 
% m_L = foreshock magnitude class %we set [2.5:0.5:4]
% d = fractal dimension %we set = 1.6
% b = b-value %we set = 1
% n_main = minimum magnitude of mainshocks used for analyses, %we set = 4
% spacing = spacing of mainshock magnitude class, %we set = 0.5

% OUTPUT variables:
%normalized number of foreshocks for each class
n_foreshocks_rc = NaN(length(m_M),length(m_L));
%number of mainshocks with foreshocks >0 for each class
n_mainshocks_rc = NaN(length(m_M),length(m_L));
%number of foreshock for each class (without normalization)
n_foreshocks_real = NaN(length(m_M),length(m_L));

% --- Mainshocks
%index of all mainshocks for each class
idx_all_main_for_each_class = cell(length(m_M),length(m_L));
%index of mainshocks with foreshocks > 0 for each class
idx_main_for_each_class = cell(length(m_M),length(m_L));

% --- Foreshocks
%number of foreshocks (also zero) of each mainshocks for each class
all_previous_event_for_each_class = cell(length(m_M),length(m_L));
%number of foreshocks (> zero) of each mainshock for each class
all_previous_event_larger_than_0 = cell(length(m_M),length(m_L));

%add subfolder "extra-functions" as the main path
addpath('/extra-functions/');

%% 1. NN of each event
% Calculate all for the j-th event
% Initialize n-sized vectors
n = length(Lat);
%nearest neighbor distance of each event
NND = NaN(n, 1);
%idx of the parent
idx_nnd = NaN(n, 1);
mag_eff = 10.^(-b*Magn);%Part of n_ij formula
Lat_km = 110.574; %as Latitude reference

for j=2:n

    Lon_km = 111.320*cos(Lat(j)*pi/180);

    r = NaN(j-1, 1);
    tt = NaN(j-1, 1);
    nn = NaN(j-1, 1);
    % Do pairwise calculation
    i_prev = 1:j-1;

    for i=i_prev

        % Distance
        x_diff_km = (Long(j) - Long(i))*Lon_km;
        y_diff_km = (Lat(j) - Lat(i))*Lat_km;
        r(i) = sqrt(x_diff_km^2 + y_diff_km^2);

        % Time difference
        tt(i) = time_rel(j) - time_rel(i);

    end

    r(r==0) = 0.1;  % [100m] about the resolution of the catalog
    tt = tt / 365.25;

    % Nearest neigbor distance
    nn = tt .* r.^d .* mag_eff(i_prev);
    [NND(j),idx_nnd(j)] = min(nn);

    clear r tt nn x_diff_km y_diff_km

end

%% 2 Find single and families using the NN distance;
% here we use log10(NND) >= -5 to define single and families, as suggested
% by Zaliapin et al, (2008)
idx_strong = idx_nnd'; %in this vector there are the index of the parents
%events for each single event

idx_strong(log10(NND) >= -5) = NaN; %Zaliapin says: weak links (large
% distances) are defined by the condition "logNND >= 10^-5";
% Put all the index with this condition = NAN;
% in this way some events do not have a parent event anymore, and they could
% be single or can start a new family.

% 2.1 Find single events and parents events
idx_events = 1:numel(Magn); %index of all events
idx_events = idx_events';

single= [];
parent = [];
for i= 1:length(idx_events)
    %if the event i has not a parent event
    if isnan(idx_strong(i)) || idx_strong(idx_events==i)== 0

        %if the events i is not a parent event and has not a parent event
        if idx_events(i)~= idx_strong
            %then it is a single
            single = [single i];

        else %if the event i is not a single and has not a parent event
            parent = [parent i]; %then it is a parent event with children
            %(it starts a family)
        end

    end
end

%check the sons of sons of each parent event, in order to
%make a family and to find the mainshock (larger event of the family)
%Search a family for each parent event and the mainshock of each family

% Ref (Zaliapin, I., and Y. Ben-Zion (2013), Earthquake clusters in southern
% California II: Classification and relation to physical properties of the
% crust, J. Geophys. Res. Solid Earth, 118, 2865–2877, doi:10.1002/jgrb.
% 50178.)

% 2.2 Find families and their mainshocks

Magn_Mainshock = []; %Magnitude of each mainshock
idx_Mainshock = []; %index of each mainshock
all_prev_event = {}; %index of foreshocks

for j = parent %run for each parent

    Family = [idx_events(idx_strong==j); j]; %take the first sons of the parent
    all_previous_event = [];

    %---- Compute FAMILY -----

    % d_link = 1 stop at the first generation of sons
    d_links(Family) =  1;   %first sons with no other sons
    d_links(j) = 0;         %first parent

    % d_link = 0 the generation of sons continues
    d_links(Family((ismember(Family,idx_strong)))) = 0; %first sons with sons

    %the following variables are used to continue the loop
    Family_2 = zeros(length(Family+1));
    c = numel(Family_2)- numel(Family);
    count = 1;

    while c >0 % until there are sons of sons, count +1

        count = count + 1; % keep track of the number of generation
        Family_2 = Family;
        for i = Family'
            %add the next generation
            Family_2 = (unique([Family_2; idx_events(idx_strong==i)]));
        end

        if numel(Family_2) > numel(Family)  %if there are new sons

            new_sons = setdiff(Family_2,Family); %take only the new sons
            d_links(new_sons) = count;           %the number of links

            %if the sons have other sons
            [~,new_sons_with_sons] = ismember(new_sons,idx_strong);
            %take their index
            new_sons_with_sons = idx_strong(new_sons_with_sons...
                (new_sons_with_sons>0));
            %put 0 as number of links
            d_links(new_sons_with_sons) = 0;

        end
        c = numel(Family_2)-numel(Family);
        Family = Family_2;
        d_links = d_links(d_links >0);

    end

    %---- MAINSHOCKS of each family -----
    %take the max magnitude of each family
    [Max_family,idx] = max(Magn(Family));
    %Magnitude of the mainshock of each family
    Magn_Mainshock = [Magn_Mainshock; Max_family];
    %index of the mainshocks
    idx_Mainshock = [idx_Mainshock; Family(idx)];

    %--- search the FORESHOCKS of each mainshock ----
    all_previous_event = (Family(Family<(idx_Mainshock(end))));
    all_prev_event(length(Magn_Mainshock)) = {all_previous_event};
    all_events = (Family);
    all_events_for_each_family(length(Magn_Mainshock)) = {all_events};

    Family = [];
    Family_2 = [];
    d_links = [];
end


%% 3. Consider only mainshocks with M >= m_main
% keep only the foreshocks sequences with mainshocks magnitude above m_main
sel = Magn_Mainshock>=m_main;
% Magnitude of mainshocks (M>= m_main)
Mainshock_larger_m_main = Magn_Mainshock(sel);
% index of mainshocks (M>= m_main) that have at least one foreshock or one
% aftershock
idx_Mainshock_larger_m_main = idx_Mainshock(sel);
% all foreshocks of each mainshock (M>= m_main)
all_prev_event_larger_m_main = all_prev_event(sel);
% new index of mainshocks > m_main
ref_ = 1:length(Mainshock_larger_m_main);

cc = 0; %as support

for mag_cl = m_M  % loop over magnitude mainshock class

    cc = cc +1; %as support to store mainshock class
    c = 0; %as support to store data for each for/aftershock class

    for L_class =  m_L % loop over for/aftershocks magnitude threshold

        c = c+1;
        n_main_4 = 0;      %as support to count the TOTAL number of mainshocks with foreshocks
        n_foresh_4 = 0;    %as support to count the TOTAL number of foreshocks (>0)
        n_foresh_for_each_main = []; %as support to save the number of foreshocks for EACH mainshock, also 0 foreshocks     
        n_foresh_larger_than_0 = []; %as support to save the number of foreshocks for EACH mainshock (foreshocks>0)

        idx_main_4 = [];  %as support to take the index of mainshocks with foreshocks

        % Obtain indices for mainshock within a magnitude threshold
        idx_ref = ref_(Mainshock_larger_m_main >= mag_cl & Mainshock_larger_m_main < mag_cl + spacing);

        %last mainshocks class
        if mag_cl == 6
            idx_ref = ref_(Mainshock_larger_m_main >= mag_cl & Mainshock_larger_m_main < mag_cl + m_main);
        end

        % --- Normalization
        %count the number of mainshocks for each foreshock magnitude
        %threshold M 2.5+, 3+, 3.5+, 4+ and divide by the number of
        %mainshocks having foreshocks in that class

        %if there are not mainshocks, break
        if isempty(idx_ref)
            n_main = [];
            n_foresh = [];
            continue
        else 
            %if there are mainshocks 
            for idx_m_4 = idx_ref

                %take the number of foreshocks for each mainshock for each
                %class               
                n_foresh_for_each_main = [n_foresh_for_each_main;...
                    nnz(Magn(all_prev_event_larger_m_main{idx_m_4}) >= L_class)];

                %if the number of foreshocks is larger than 0
                if nnz(Magn(all_prev_event_larger_m_main{idx_m_4}) >= L_class) > 0

                    %if the mainshock of this class have foreshocks in this
                    %class, count the number of mainshocks
                    n_main_4 = n_main_4 +1;
                    %TOTAL number of foreshocks of this magnitude mainshock class
                    n_foresh_4 = n_foresh_4 + nnz(Magn(all_prev_event_larger_m_main{idx_m_4}) >= L_class);
                    %take the index of mainshocks with foreshocks for this class
                    idx_main_4 = [idx_main_4; idx_m_4];
                    %take the number of the foreshocks (>0) of EACH mainshock for
                    %this class
                    n_foresh_larger_than_0 = [n_foresh_larger_than_0;...
                        nnz(Magn(all_prev_event_larger_m_main{idx_m_4}) >= L_class)];

                end
            end
        end

        %--- All mainshocks M>m_main
        %index of all mainshocks (M>=m_main) for each class
        idx_all_main_for_each_class(cc,c) = {idx_Mainshock_larger_m_main(idx_ref')};
        %number of foreshocks (also zero) of each mainshock for each class
        %(it is empty when we don't have mainshocks in that class)
        all_previous_event_for_each_class(cc,c) = {n_foresh_for_each_main};

        %--- All mainshocks M>m_main with foreshocks (>0)
        %index of the mainshocks (M>=m_main) that have foreshocks for each class
        idx_main_for_each_class(cc,c) = {idx_Mainshock_larger_m_main(idx_main_4)};
        %number of foreshocks (> zero) of each mainshock for each class
        %(it is empty when we don't have mainshocks with foreshocks in that class)
        all_previous_event_larger_than_0(cc,c) = {n_foresh_larger_than_0};

        %--- DATA for I analysis: normalized number of foreshock for each
        %class
        %divide the number of foreshocks by the number of mainshocks that
        %have foreshocks
        n_mainshocks_rc(cc,c) = n_main_4;
        n_foreshocks_rc(cc,c) = n_foresh_4 /n_main_4;
        n_foreshocks_real(cc,c) = n_foresh_4;


    end

end
end



