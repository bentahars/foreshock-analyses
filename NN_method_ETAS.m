%% Declustering using NN-method for synthetic catalogs
% Here we use the NN formulation of Zaliapin et al., (2008) to find
% the parent of each event for each synthetic catalog

% TO NOTE: this function take time to run due to declustering of each ETAS
% catalog

function [idx_main_for_each_class_synt_cat,...
    all_previous_event_for_each_class_synt_cat,...
    all_previous_event_larger_than_0_synt_cat,n_mainshocks_synt_cat,...
    n_foreshocks_synt_cat,n_foreshocks_real_synt_cat] ...
    = NN_method_ETAS(Repcat,d,b,n_synt_cat,m_L,m_M,m_main,spacing)

% INPUT variables:
% m_M = mainshock magnitude class %we set [4:0.5:6] note the last class has
%       not a right edge (all mainshocks M>6 are part of the last class).
% m_L = foreshock magnitude class %we set [2.5:0.5:4]
% d = fractal dimension %we set = 1.6
% b = b-value %we set = 1
% n_main = minimum magnitude of mainshocks used for analyses, %we set = 4
% spacing = spacing of mainshock magnitude class, %we set = 0.5
% Repcat = cell array with all synthetic catalogs REF= README.file;
% Repcat = cell(1, n_synt_cat); where n_synt_cat is the number of synthetic catalogs
% n_synt_cat = number of synthetic catalogs;

% OUTPUT variables:
%normalized number of foreshocks for each class
n_foreshocks_synt_cat = cell(length(m_M),length(m_L),n_synt_cat);
%number of mainshocks with foreshocks >0 for each class
n_mainshocks_synt_cat = cell(length(m_M),length(m_L),n_synt_cat);
%number of foreshock for each class (without normalization) for each cat
n_foreshocks_real_synt_cat = NaN(length(m_M),length(m_L));

% --- Mainshocks
%index of all mainshocks for each class
idx_all_main_for_each_class_synt_cat = cell(length(m_M),length(m_L),n_synt_cat);
%index of mainshocks with foreshocks > 0 for each class
idx_main_for_each_class_synt_cat = cell(length(m_M),length(m_L),n_synt_cat);

% -- Foreshocks 
%number of foreshocks (also zero) of each mainshocks for each class
all_previous_event_for_each_class_synt_cat = cell(length(m_M),length(m_L),n_synt_cat);
%number of foreshocks (> zero) of each mainshock for each class
all_previous_event_larger_than_0_synt_cat = cell(length(m_M),length(m_L),n_synt_cat);

%add subfolder "extra-functions" as the main path
addpath('/extra-functions/');

%% 1. NN of each event

%nearest neighbo distance of each event
nnd_synt_cat = cell(1,n_synt_cat);
%idx of the parent
idx_strong_synt_cat = cell(1,n_synt_cat);

for p = 1:n_synt_cat


    Lat = Repcat{p}(:,7);
    Long = Repcat{p}(:,8);
    Magn = Repcat{p}(:,10);
    time = Repcat{p}(:,[1:6]);
    time = datetime(time,'Format',"uuuu-MM-dd' 'HH:mm:ss.SSS");
    time_rel = datenum (time);

    %(1) input variables
    n = length(Lat);
    % Calculate all for the j-th event
    % Prepare time information

    % Initialize n-sized vectors
    NND = NaN(n, 1);
    idx_nnd = NaN(n, 1);
    mag_eff = 10.^(-b*Magn);    %Part of n_ij formula

    Lat_km = 110.574;           %as Latitude reference

    for j=2:n

        Lon_km = 111.320*cos(Lat(j)*pi/180);

        r = NaN(j-1, 1);
        tt = NaN(j-1, 1);
        nn = NaN(j-1, 1);
        % Do pairwise calculation
        i_prev = 1:j-1;

        for i=i_prev

            % Distance
            x_diff_km = (Long(j) - Long(i))*Lon_km;
            y_diff_km = (Lat(j) - Lat(i))*Lat_km;

            r(i) = sqrt(x_diff_km^2 + y_diff_km^2);

            % Time difference
            tt(i) = time_rel(j) - time_rel(i);

        end

        r(r==0) = 0.1;  % [100m] about the resolution of the catalog;
        % maybe scale with random noise

        tt = tt / 365.25;

        % Nearest neigbor distance
        nn = tt .* r.^d .* mag_eff(i_prev);
        [NND(j),idx_nnd(j)] = min(nn);

        clear tt r nn y_diff_km x_diff_km
    end


    NND(NND==0) = nan;  % this is important to compute the minimum
    logNND = log10(NND(2:end));
    logNND = sort(logNND);


    idx_strong = idx_nnd'; %in this variable there are the index of the parents
    %events for each single event

    idx_strong(log10(NND) >= -5) = NaN; %Zaliapin says: weak links (large distances) are defined by the
    %condition "logNND >= 10^-5";I put all the index with this condition = NAN;
    %in this way some events do not have a parent event anymore, and they could
    %be single or can start a new family.

    nnd_synt_cat(p) = {NND};
    idx_strong_synt_cat(p) = {idx_strong};

end

%% 2 Find single and families using the NN distance;
% here we use log10(NND) >= -5 to define single and families, as suggested
% by Zaliapin et al, (2008)

%mainshock of each families of each catalog
Mainshocks_for_each_synth_cat_Zal = cell(1,n_synt_cat);
%index of the mainshocks
idx_Mainshocks_synth_cat = cell(1,n_synt_cat);
%index of foreshock of each mainshock
all_prev_event_synth_cat = cell(1,n_synt_cat);
%index of all events for each families
all_events_for_each_family_synt_cat = cell(1,n_synt_cat);

for p=1:n_synt_cat

    % Single events and parents events
    Lat = Repcat{p}(:,7);
    Magn = Repcat{p}(:,10);
    idx_strong = idx_strong_synt_cat{p};

    idx_events = 1:numel(Lat); %index of all events
    idx_events = idx_events';

    single= [];
    parent = [];
    for i= 1:length(idx_events)
        %if the event i has not a parent event
        if isnan(idx_strong(i)) || idx_strong(idx_events==i)== 0

            %if the events i is not a parent event and has not a parent event
            if idx_events(i)~= idx_strong
                %it is a single
                single = [single i];

            else %if the event i is not a single and has not a parent event
                %then it is a parent event with sons
                parent = [parent i];
                %(it starts a family)
            end

        end
    end

    % 2.2 Find families and their mainshocks

    Mainshock = [];
    idx_Mainshock = [];

    all_prev_event = {};
    d_average = zeros(1,length(parent));

    for j = parent %run for each parent

        %take the first sons of the parent
        Family = [idx_events(idx_strong==j); j];
        all_previous_event = [];

        %---- Compute AVERAGE LEAF DEPTH and make FAMILY -----

        d_links(Family) =  1;   %first sons with no child
        d_links(j) = 0;         %first parent
        d_links(Family((ismember(Family,idx_strong)))) = 0; %first sons with child
        Family_2 = zeros(length(Family+1));
        c = numel(Family_2)- numel(Family);
        count = 1;

        while c >0

            %keep track of the number of generation
            count = count + 1;
            Family_2 = Family;
            for i = Family'
                %add the next generation
                Family_2 = (unique([Family_2; idx_events(idx_strong==i)]));
            end

            %if there are new sons
            if numel(Family_2) > numel(Family)
                %take only the new sons
                new_sons = setdiff(Family_2,Family);
                %put the number of links
                d_links(new_sons) = count;

                %search new sons with other sons
                %if the sons have other sons
                [~,new_sons_with_sons] = ismember(new_sons,idx_strong);
                %take their index
                new_sons_with_sons = idx_strong(new_sons_with_sons...
                    (new_sons_with_sons>0));
                %put 0 as number of links
                d_links(new_sons_with_sons) = 0;

            end
            c = numel(Family_2)-numel(Family);
            Family = Family_2;
            d_links = d_links(d_links >0);

        end

        %---- search the MAINSHOCKS of each family -----
        %take the max magnitude of each family
        [Max_family,idx] = [max(Magn(Family))];
        %Magnitude of the mainshocks of each family
        Mainshock = [Mainshock; Max_family];
        %index of the mainshocks
        idx_Mainshock = [idx_Mainshock; Family(idx)];


        %--- search the FORESHOCKS of each mainshock ----
        all_previous_event = [Family(Family<(idx_Mainshock(end)))];
        all_prev_event(length(Mainshock)) = {all_previous_event};
        all_events = (Family);
        all_events_for_each_family(length(Mainshock)) = {all_events};


        Family = [];
        Family_2 = [];
        d_links = [];
    end

    Mainshocks_for_each_synth_cat_Zal(p) = {Mainshock};
    idx_Mainshocks_synth_cat(p) = {idx_Mainshock};
    all_prev_event_synth_cat(p) = {all_prev_event};
    all_events_for_each_family_synt_cat(p) = {all_events_for_each_family};

end

%% 3. Consider only mainshocks with M >= m_main and count maishocks and foreshocks

for p=1:n_synt_cat

    %Take variable
    Mainshock = Mainshocks_for_each_synth_cat_Zal{p};
    idx_Mainshock = idx_Mainshocks_synth_cat{p};
    all_prev_event = all_prev_event_synth_cat{p};
    Magn = Repcat{p}(:,10);

    %keep only the foreshocks sequences with mainshocks magnitude above m_main
    sel = Mainshock>=m_main;
    Mainshock_major_4 = Mainshock(sel);
    idx_Mainshock_major_4 = idx_Mainshock(sel);
    all_prev_event_major_4 = all_prev_event(sel);

    ref_ = 1:length(Mainshock_major_4);   %Index of mainshocks > m_main

    cc = 0; %as support
    for mag_cl = m_M  % loop over magnitude mainshock class

        cc = cc +1; %as support to store mainshock class
        c = 0; %as support to store data for each for/aftershock class

        for L_class =  m_L % loop over for/aftershocks magnitude class

            c = c+1;
            n_main = 0;      %as support to count the TOTAL number of mainshocks with foreshocks
            n_foresh = 0;    %as support to count the TOTAL number of foreshocks (>0)

            n_foresh_for_each_main = []; %as support to count the number of foreshocks for EACH mainshock, also 0 foreshocks       
            n_foresh_larger_than_0 = []; %as support to count the number of foreshocks for EACH mainshock (foreshocks>0)

            idx_main_4 = [];  %as support to take the index of mainshocks with foreshocks

            %indices for events within a magnitude threshold
            idx_ref = ref_(Mainshock_major_4 >= mag_cl & Mainshock_major_4 < mag_cl + spacing);

            %last mainshock class
            if mag_cl == 6
                idx_ref = ref_(Mainshock_major_4 >= mag_cl & Mainshock_major_4 < mag_cl + m_main);
            end

            % --- Normalization 
            %count the number of mainshocks for each foreshocks magnitude
            %threshold M 2.5+, 3+, 3.5+, 4+

            %if there are not mainshocks, break
            if isempty(idx_ref)
                n_main = [];
                n_foresh = [];
                continue
            else
                %if there are mainshocks 
                for idx_m_4 = idx_ref

                    %take the number of foreshocks for each mainshock for each
                    %class
                    n_foresh_for_each_main = [n_foresh_for_each_main;...
                        nnz(Magn(all_prev_event_major_4{idx_m_4}) >= L_class)];

                    %if the number of foreshocks is larger than 0
                    if nnz(Magn(all_prev_event_major_4{idx_m_4}) >= L_class) > 0

                        %if the mainshock of this class have foreshocks in this
                        %class, count the number of mainshocks
                        n_main = n_main +1;
                        %take the number of foreshocks of this magnitude mainshock class
                        n_foresh = n_foresh + nnz(Magn(all_prev_event_major_4{idx_m_4}) >= L_class);
                        %take the index of mainshocks with foreshocks for this class
                        idx_main_4 = [idx_main_4; idx_m_4];
                        %take the number of the foreshocks (>0) of each mainshock for
                        %this class
                        n_foresh_larger_than_0 = [n_foresh_larger_than_0;...
                            nnz(Magn(all_prev_event_major_4{idx_m_4}) >= L_class)];

                    end
                end

            end

            %--- All mainshocks M>m_main
            %index of all mainshocks (M>=m_main) for each class
            idx_all_main_for_each_class_synt_cat(cc,c,p) = {idx_Mainshock_major_4(idx_ref')};
            %number of foreshocks (also zero) of each mainshock for each class
            %(it is empty when we don't have mainshocks in that class)
            all_previous_event_for_each_class_synt_cat(cc,c,p) = {n_foresh_for_each_main};

            %--- All mainshocks M>m_main with foreshocks (>0)
            %index of the mainshocks (M>=m_main) that have foreshocks for each class
            idx_main_for_each_class_synt_cat(cc,c,p) = {idx_Mainshock_major_4(idx_main_4)};
            %number of foreshocks (> zero) of each mainshock for each class
            %(it is empty when we don't have mainshocks with foreshocks in that class)
            all_previous_event_larger_than_0_synt_cat(cc,c,p)= {n_foresh_larger_than_0};

            %--- DATA for I analysis: normalized number of foreshock for each
            %class
            %divide the number of foreshocks by the number of mainshocks that
            %have foreshocks
            n_foreshocks= n_foresh/n_main;
            n_mainshocks= n_main;

            %save data for each synthetic catalog p
            n_foreshocks_synt_cat(cc,c,p) = {n_foreshocks};
            n_mainshocks_synt_cat(cc,c,p) = {n_mainshocks};
            n_foreshocks_real_synt_cat(cc,c,p) = (n_foresh);

        end

    end

end

end