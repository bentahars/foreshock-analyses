%% Compute the eCDF of number of foreshocks for each class
% We consider only the sequences that have foreshocks (>0)

function [idx_main_with_anomalous_foresh,idx_main_for_each_class,...
    anomalous_sequences,percent_selected,m_M,m_L] = ...
    Alternative_TEST1(all_previous_event_larger_than_0,...
    all_previous_event_larger_than_0_synt_cat,percent_selected,...
    idx_main_for_each_class,n_mainshocks_rc,m_M,m_L)


% INPUT variables are obtained after running NN_method.m or STW_method.m
% all input variables are explained in that file.m
% To set:
% percent_selected = percentile % we set 99th or 95th percentile

% OUTPUT variables
%idx of anomalous mainshocks
idx_main_with_anomalous_foresh = cell(length(m_M),length(m_L));
%number of anomalous sequences
anomalous_sequences = [];

%add subfolder "extra-functions" as the main path
addpath('/extra-functions/');

%% Start analysis

%decisional test for the real catalog
h_test_REAL = NaN(length(m_M),length(m_L));
%percentile for the real catalog
percentile_cdf_ETAS = NaN(length(m_M),length(m_L));
percentile_cdf_real = NaN(length(m_M),length(m_L));

% Histogram for each mainshock and foreshocks class
figure('Position',[0 0 1000 1000],'Color',[1 1 1]);
tlo = tiledlayout(length(m_M),length(m_L),'Padding', 'none',...
    'TileSpacing', 'compact');

c = 0; %number of cicles
range_on_figures = (1:length(m_L):length(m_L)*length(m_M));


for rows = 1:length(m_M)
    for columns = 1:length(m_L)

        c = c+1;
        ax_lgd = nexttile(c);
        ax(c) = nexttile(c);

        %this condition is for empty values
        if isnan(vertcat(all_previous_event_larger_than_0_synt_cat{rows,columns,:}))
            continue
        end

        %--------1 Plot the CDF of ETAS
        %histrogram for each magnitude class
        hold on
        ecdf(vertcat(all_previous_event_larger_than_0_synt_cat{rows,columns,:}));
        h_ecdf = get(gca,'children');
        set(h_ecdf(1),'color','k','Linestyle','--','LineWidth',0.8);
        hold on

        %-------- 2.Compute 99 percentile of the CDF-ETAS
        percentile_cdf_ETAS(rows,columns) =(prctile...
            (vertcat(all_previous_event_larger_than_0_synt_cat{rows,columns,:}),percent_selected));
        xline(percentile_cdf_ETAS(rows,columns),'Linestyle','--','LineWidth',1.5)
        hold on

        if isempty(cell2mat(all_previous_event_larger_than_0(rows,columns)))
            continue
        end

        %-------- 2.1 Compute 99 percentile of the CDF-real
        percentile_cdf_real(rows,columns) =(prctile...
            (cell2mat(all_previous_event_larger_than_0(rows,columns)),percent_selected));

        %-------- 3 Compare both percentile (real and etas)
        % H_value
        if percentile_cdf_real(rows,columns)  <= percentile_cdf_ETAS(rows,columns)
            h_test_REAL(rows,columns)  = 0; %do not reject the null hypot
            numel_of_anom_sequ = 0;
        else
            h_test_REAL(rows,columns)  = 1; %reject the null hypothesis

            %--------------- 4.Anomalous sequences
            % Keep as anomalous the sequences with a number of
            % foreshocks above the ETAS percentile
            % take the mainshocks for each class that have anomalous number
            % of foreshocks (larger than percentile)

            idx_main_anom = [];

            for z_main = find(cell2mat(idx_main_for_each_class(rows,columns)))'
                %count the number of foreshocks for each class
                %compare with the xx percentile
                if all_previous_event_larger_than_0{rows,columns}(z_main)...
                        >= ceil(percentile_cdf_ETAS(rows,columns))
                    idx_main_anom = [idx_main_anom;...
                        idx_main_for_each_class{rows,columns}(z_main)];%<--take the index of the mainshock
                    %that have a number of foreshocks larger than the xx-percentile
                end
            end
            %anomalous sequences for each class
            idx_main_with_anomalous_foresh{rows,columns} = (idx_main_anom);
             %number of anomalous sequences for each class (for annotation plot)
            numel_of_anom_sequ = length(idx_main_anom);
        end
        %count the total number of anomlous sequences from this test
        anomalous_sequences = unique(vertcat(idx_main_with_anomalous_foresh{:,:}));

        %--------- 3. Plot the CDF of REAL as solid lone
        %add black line for the rel catalog, that is the larger number of
        %foreshocks
        color_for_anom = hex2rgb('#ec3c0d');% REF. for function:Chad Greene (2022). 
        % rgb2hex and hex2rgb (https://www.mathworks.com/matlabcentral/fileexchange/46289-rgb2hex-and-hex2rgb),
        % MATLAB Central File Exchange. Retrieved March 12, 2022. 

        %if there not are anomalous sequences
        if isempty(idx_main_with_anomalous_foresh{rows,columns})
            hold on
            ecdf(cell2mat(all_previous_event_larger_than_0(rows,columns)));
            h_ecdf = get(gca,'children');
            set(h_ecdf(1),'color','k','Linestyle','-','LineWidth',0.7);
            
            hold on
            xline(percentile_cdf_real(rows,columns),'k-','LineWidth',1.5)

            %just to show red line in the legend
            hold on
            plot(nan,nan,'color',color_for_anom,'Linestyle','-','LineWidth',1.5)
        %if there are anomalous sequences
        else
            hold on
            ecdf(cell2mat(all_previous_event_larger_than_0(rows,columns)));
            h_ecdf = get(gca,'children');
            set(h_ecdf(1),'color','k','Linestyle','-','LineWidth',0.7);
            
            %just to show black line in the legend
            hold on
            plot(nan,nan,'k-','LineWidth',1.5)
            
            hold on
            xline(percentile_cdf_real(rows,columns),'color',color_for_anom,...
                'Linestyle','-','LineWidth',1.5) 
        end

        %Label
        yticks([0 0.5 1])
        bb = max(cell2mat(all_previous_event_larger_than_0(rows,columns)))+...
            percentile_cdf_ETAS(rows,columns)+...
            (30*percentile_cdf_ETAS(rows,columns)/100);
        xlim([0 bb])
        xlabel('')
        ylabel('')

        %Legend (add legend only at the first cicle)
        if rows == 1
            if columns == 1
                xlabel(tlo, 'Number of foreshocks, N_F','FontWeight','bold','FontSize',12)
                lg  = legend(ax_lgd,'CDF of N^E^T^A^S_F',...
                    join(sprintf('%g^t^h percentile of CDF of N^E^T^A^S_F',percent_selected)),...
                    'CDF of N_F^r^e^a^l',...
                    join(sprintf('%g^t^h percentile of CDF of N_F^r^e^a^l',percent_selected)),...
                    join(sprintf('%g^t^h percentile of CDF of N_F^r^e^a^l (Anomalies)',percent_selected)),...
                    'Orientation','horizontal','NumColumns',5,'FontSize',11);
                lg.Layout.Tile = 'North'; % <-- Legend placement with tiled layout
            end
        end

        if c>=1 && c<=length (m_L)
            Title_names = join(["m_F ≥ ", m_L(columns)]);
            title(Title_names,'FontWeight','bold','FontSize',12)
        end

        %Loop to put the y axis only to the left figures
        if c == range_on_figures(end)
            y_label_names_last = join(sprintf('m_M ≥ %.1f',m_M(end)));
            ylabel(y_label_names_last,'FontWeight','bold','FontSize',12)
        elseif ismember(c,range_on_figures)
            y_label_names = join(sprintf('%.1f ≤ m_M < %.1f',m_M(rows),m_M(rows+1)));
            ylabel(y_label_names,'FontWeight','bold','FontSize',12);
        end

        %----- 5. Annotation

        %Annotation for number of Anomalous Sequences
        if numel_of_anom_sequ > 0
            n_anom_seq = sprintf('N_A_F_S = %.f',numel_of_anom_sequ);
        end

        if numel_of_anom_sequ == 0
            n_anom_seq = sprintf('N_A_F_S = 0');
        end

        n_M = sprintf('N_M = %.f', n_mainshocks_rc(rows,columns));
        hold on

        %Texbox with all annotation
        str = {n_anom_seq,'', '','', n_M};
        annotation('textbox','String',str,...
            'FitBoxToText','on','Color','k',...
            'FontSize',10,'Position',ax(c).Position,...
            'HorizontalAlignment','right','Vert','middle',...
            'EdgeColor','none','Margin',2);
        hold on

    end
end

end